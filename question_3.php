<?php

namespace wpdb\job_application;

/**
 * Get a random number of apples, try to internationalize it
 * make sure it is a whole number by rounding it.
 * return $apple_count ensuring that it is an int
 * @return int
 */
function get_apple_count_i18n() {
	$apple_count = __( round( rand( 0, 5 ) ), 'test' );
	return (int) $apple_count;
}

echo esc_html( get_apple_count_i18n() );