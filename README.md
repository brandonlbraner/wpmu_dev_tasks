# WPMU Developer Tasks

### Question 1
How would you improve this code?
```
<?php
global $wpdb;
$query = "SELECT * FROM $wpdb->postmeta WHERE meta_key = '" .
$my_option_name . "'";
$results = $wpdb->get_results( $query );
```

**Question 1 Answer**
Code file question_1.php

By using the WP_Query object we get more control over the performance of our query.
We are allowed to set pagination, caches we do or don't set, how many results to return etc.
I built a function that accepts what I think are the minimum number of arguments to a WP_Query object
to get back the posts data that you want. It can easily be extended if needed as well as checks to ensure key pieces
of information are provided before running the query.  

Also in the query above there is no sanitization done, so if you were to use $wpdb->get_results you should pass in a properly 
sanatized query that has ben ran through $wpdb->prepare


### Question 2
Now​ ​take​ ​a​ ​go​ ​at​ ​improving​ ​this​ ​code,​ ​reckon​ ​you​ ​can
figure​ ​it​ ​out?

```
<!-- Assume $your_name holds the previously submitted answer (if any), or
empty string -->
<label>
<span><?php _e('Your name', 'test'); ?</span>
<input type="text" name="your_name" value="<?php echo
$your_name; ?>" />
</label>
```

**Question 2 Answer**

Code file question_2.php 

Here are the couple things that stood out to me right away.
1. _e does not have a closing PHP bracket
2. input is inside of label. This maybe fine but for accessability you would need to so some special stuff ot make it work. Seems easier to me just to move it outside of it.
3. label should have a for attribute for accessibility
4. $your_name needs to be sanitized
5. Also should check to see if $your_name is set, and if it isn't make sure it is at least set to nothing


### Question 3
Prepare​ ​the​ ​following​ ​code​ ​for​ ​localization,​ ​as​ ​best​ ​and
concise​ ​as​ ​you​ ​can.​ ​Use​ ​'test'​ ​as​ ​your​ ​translation​ ​domain

```
<?php
$apples = rand(0,5);
echo "We have {$apples} apples!";
```

**Question 3 Answer**

Code in question_3.php


### Question 4

Nothing​ ​is​ ​ever​ ​perfect,​ ​right?
Tell us what we’re doing in this code, and how can it be improved?

```
<?php
// Assume user privileges and nonce are correctly checked
$options = array();
$options['option1'] = isset( $_POST['option1'] ) ? $_POST['option1'] : '';
update_option('my_option_key', $options);
```

**Question 4 Answers**

No code file for this question

1. The $_POST value is not being sanatized before it is put into the options array
2. Should probably retrieve the option first and update it vs overwritting it as there maybe other information stored
3. Is empty something we want stored? If so a better value maybe better such as false to a bit easier comparrison later.
If we don't want empty values  we should be throwing an error or exception at this point or returning and not saving the option.
4. I always like to make sure the options table is the correct place for something. The options table can become so blaoted so quickly. It more than likely is but always good to double check


### Question 5

Can​ ​you​ ​think​ ​of​ ​a​ ​scenario​ ​where​ ​code​ ​such​ ​as​ ​this​ ​would
be​ ​an​ ​anti-pattern?

```
<?php
// file: uninstall.php
if ( is_multisite() ) {
    global $wpdb;
    $blogs = $wpdb->get_results( "SELECT blog_id FROM {$wpdb->blogs}", ARRAY_A );
    if ( ! empty( $blogs ) ) {
        foreach ( $blogs as $blog ) {
            switch_to_blog( $blog['blog_id'] );
            delete_option( 'option_name' );
        }
    }
} else {
    delete_option( 'option_name' );
}
```

***Question 5 Answers***

Code file question_5.php

If you were going to use the code above the first thing I would do is flip the outside conidtional around.
You have a lot of code inside an if statement that doesn't need to be, this creates unnessarcy nesting.
Also if you have a very large network you could be looping over a bunch of sites. 

In the provided code file I provided how I would refactor the code provided as well as another function on
how I would rewrite it to be a bit more peformant. 


### Question 6

Can​ ​you​ ​explain​ ​what​ ​might​ ​go​ ​wrong​ ​with​ ​this​ ​bit​ ​of javascript​ ​code?

```
if ("object" === typeof a && "stuff" in a) { alert(a.stuff); }
```

***Question 6 Answer***

No code file

There is no santiziation being done on stuff. If someone was able to preform a xxs on the
site they could alert any message to a user. Could be a huge secruity risk.


### Question 7

Can​ ​you​ ​spot​ ​a​ ​problem​ ​with​ ​this​ ​bit​ ​of​ ​javascript​ ​code?

```
// file: my-js-file.js
function get_number (string) {
    return parseInt(string);
}
```

***Question 7 Answer***

No code file

You should check to make sure that parseInt(string) doesn't return NaN


### Question 8

​Can​ ​you​ ​spot​ ​a​ ​problem​ ​with​ ​this​ ​bit​ ​of​ ​code?

```
<?php
function is_not_ready_to_go() {
    return empty( get_site_option( 'my_plugin_option' ) );
}
```


***Question 8 Answer***

No code file

I think it depends on the context of how the function is used.
If you are looking to use it to see if that option exists and want true to be returned if it does not
everything works fine.

If you are expecting an answer of the value it returns that won't work because get_site_option would still return an array
with a blank value and empty would not see that as empty


### Question 9


How​ ​would​ ​you​ ​refactor​ ​this​ ​bit​ ​of​ ​code​ ​to​ ​be​ ​more​ ​concise​ ​in WordPress​ ​context

```
<?php
// Assume $map looks like this:
// $map = array(
	// array( 'name' => 'Name 1', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 2', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 3', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 4', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 5', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 6', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 7', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 8', 'something else' => 'whatever' ),
	// array( 'name' => 'Name 9', 'something else' => 'whatever' ),
// );
$names = array();
foreach ( $map as $data ) {
	$names[] = $data['name'];
}
```

***Question 9 Answer***

Code file: question_9.php

I would create a seperate function that contains an array_map over the $map array and returns name.


### Question 10

How​ ​would​ ​you​ ​refactor​ ​this​ ​bit​ ​of​ ​code​ ​in​ ​a​ ​plugin​ ​you're assigned?

```
<?php
// Assume $path_piece1 = '/test/dir1/';
// Assume $path_piece2 = 'subdir2\file.txt';
$path = str_replace( '\\', '/', $path_piece1 . $path_piece2 );
```

***Question 10 Answer***

No code file

First I would go to wherever the file paths are defined and normalize the structure.
You have some paths with forward slashes other with backslashes and you can't be sure which
operating system your plugin is going to be on. PHP ships with the constant `DIRECTORY_SEPERATOR` so that should be used

Also this means strip off all / or \ from the front and back of the string so my paths would now be

```
<?php
$path_piece1 = 'test'.DIRECTORY_SEPERATOR.'dir1';
$path_piece2 = 'subdir2'.DIRECTORY_SEPERATOR.'file.txt';
```

Since you can never really trust people to keep data in the same format I would also `trim(DIRECTORY_SEPERATOR, $path_piece)`; before using it so my `$path` variable would be

```
function build_path($path_peieces = []) {
    join(DIRECTORY_SEPERATOR, trim(DIRECTORY_SEPERATOR, $path_pieces))
}
$path = build_path([path_piece1, path_piece2]); 

```


### Question 11

​Is​ ​there​ ​anything​ ​you​ ​would​ ​improve​ ​in​ ​this​ ​piece​ ​of code?

```
<?php
// Assume js/my-custom-posts-script.js is present, and does something on posts list page (edit.php)
function my_admin_scripts_inclusion_proc() {
	wp_enqueue_script( 'my-custom-posts-script', plugins_url( 'js/my-custom-posts-script.js', __FILE__ ) );
}

add_action( 'admin_enqueue_scripts', 'my_admin_scripts_inclusion_proc' );
```

***Question 11 Answer***

No code file

If this script is only needed on the edit.php page I would wrap it in a conidtional to only have it enqueued on that page
so it is not loaded on other pages.