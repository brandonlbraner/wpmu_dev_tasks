<?php
// Assume $map looks like this:
$map = array(
	array( 'name' => 'Name 1', 'something else' => 'whatever' ),
	array( 'name' => 'Name 2', 'something else' => 'whatever' ),
	array( 'name' => 'Name 3', 'something else' => 'whatever' ),
	array( 'name' => 'Name 4', 'something else' => 'whatever' ),
	array( 'name' => 'Name 5', 'something else' => 'whatever' ),
	array( 'name' => 'Name 6', 'something else' => 'whatever' ),
	array( 'name' => 'Name 7', 'something else' => 'whatever' ),
	array( 'name' => 'Name 8', 'something else' => 'whatever' ),
	array( 'name' => 'Name 9', 'something else' => 'whatever' ),
);


function retrieve_name( $data ) {
	if ( isset( $data['name'] ) ) {
		return $data['name'];
	}

	return '';
}

$names = array_map( 'retrieve_name', $map );
