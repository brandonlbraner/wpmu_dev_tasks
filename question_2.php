<!-- Assume $your_name holds the previously submitted answer (if any), or empty string -->

$your_name_value = <?php ( isset( $your_name ) && 0 != strlen( $your_name ) ) ? $your_name : 'Please enter your name'; ?>
<label for="your_name"> <?php _e( 'Your name', 'test' ); ?> </label>
<input type="text" name='your_name' value="<?php echo esc_attr( $your_name_value ); ?>">