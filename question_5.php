<?php

/**
 * using the code from the provided example
 * this read a little cleaner
 */


// file: uninstall.php
global $wpdb;

if ( ! is_multisite() ) {
	delete_option( 'option_name' );
}

$blogs = $wpdb->get_results( "SELECT blog_id FROM {$wpdb->blogs}", ARRAY_A );
if ( ! empty( $blogs ) ) {
	foreach ( $blogs as $blog ) {
		switch_to_blog( $blog['blog_id'] );
		delete_option( 'option_name' );
	}
}


/**
 * use get_sites
 */

function delete_option_on_all_sites( $option_name = '', $offset = 0, $number_of_sites = 100 ) {

	$args = [
		'fields' => 'ids',
		'number' => absint( $number_of_sites ),
		'offset' => absint( $number_of_sites + $offset ),
	];

	$sites = get_sites( $args );
	if ( 0 != count( $sites ) ) {
		foreach ( $sites as $site ) {
			// call delete_blog_option to avoid having to manually switch to blog
			delete_blog_option( $site['id'], $option_name );
		}
		//increment offset value
		$new_offset = absint( $number_of_sites, $offset );
		//recursvily call function
		delete_option_on_all_sites( $option_name, $new_offset );
	}
}